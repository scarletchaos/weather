import requests
from pydantic import BaseModel, ValidationError
from typing import List, Optional
import json


class Coordinates(BaseModel):
    lon: float
    lat: float


class WeatherData(BaseModel):
    id: int
    main: str
    description: str
    icon: str


class Main(BaseModel):
    temp: float
    feels_like: float
    temp_min: Optional[float] = None
    temp_max: Optional[float] = None
    pressure: float
    humidity: float
    sea_level: Optional[float] = None
    grnd_level: Optional[float] = None


class Wind(BaseModel):
    speed: float
    deg: Optional[float] = None
    gust: Optional[float] = None


class Snow(BaseModel):
    h1: Optional[float] = None
    h3: Optional[float] = None


class Rain(BaseModel):
    h1: Optional[float] = None
    h3: Optional[float] = None


class Clouds(BaseModel):
    all: int


class Sys(BaseModel):
    type: int
    id: int
    country: str
    sunrise: int
    sunset: int


class OpenWeatherData(BaseModel):
    coord: Coordinates
    weather: List[WeatherData]
    base: str
    main: Main
    visibility: int
    wind: Wind
    rain: Optional[Rain] = None
    snow: Optional[Snow] = None
    clouds: Clouds
    dt: int
    sys: Sys
    timezone: int
    id: int
    name: str
    cod: int


response = requests.get('http://api.openweathermap.org/data/2.5/weather?q=Zhukovsky&appid=5e65ef3a16cc819ff9ba4b18b70a2'
                        'cf9')
try:
    data = OpenWeatherData.parse_raw(response.content)
except ValidationError as err:
    print(err.json())
else:
    print(data)
